Summary: NixTux vendor configuration files for apt
Name: nixtux-repo
Version: 0.4
Release: alt1
License: GPL
Group: System/Configuration/Packaging
Url: https://gitlab.com/nixtux-packaging/nixtux-repo.git
BuildArch: noarch

Source0: nixtux-repo.list
Source1: nixtux-keys.list
Source2: mikhailnov_pub.gpg
Source3: nixtux-keys.filetrigger

Packager: Mikhail Novosyolov <mikhailnov@dumalogiya.ru>

Requires: alt-gpgkeys

%description
This package contains vendor configuration for NixTux repositories.

%prep
#%setup

%install
mkdir -p %buildroot%_sysconfdir/apt/sources.list.d
mkdir -p %buildroot%_sysconfdir/apt/vendors.list.d
mkdir -p %buildroot%_prefix/lib/rpm
mkdir -p %buildroot%_datadir/nixtux-repo/public-keys/
install -p -m644 %{SOURCE0} %buildroot%_sysconfdir/apt/sources.list.d/nixtux-repo.list
install -p -m644 %{SOURCE1} %buildroot%_sysconfdir/apt/vendors.list.d/nixtux-keys.list
# /usr/share/nixtux-repo/public-keys/mikhailnov_pub.gpg
install -p -m644 %{SOURCE2} %buildroot%_datadir/nixtux-repo/public-keys/mikhailnov_pub.gpg
install -p -m755 %{SOURCE3} %buildroot%_prefix/lib/rpm/nixtux-keys.filetrigger

%post
# posinstall script
echo "/usr/lib/alt-gpgkeys/pubring.gpg" | /usr/lib/rpm/nixtux-keys.filetrigger

%preun
# preuninstall script
# http://meinit.nl/rpm-spec-prepostpreunpostun-argument-values
case "$1" in
	0)
		# This is an un-installation.
		#gpg --homedir /usr/lib/alt-gpgkeys --batch --yes --delete-key 55E84CE1 || true
		/usr/lib/rpm/nixtux-keys.filetrigger remove_keys
	;;
	1)
		# This is an upgrade.
		# Do nothing.
		:
	;;
esac

%files
%config %_sysconfdir/apt/sources.list.d/nixtux-repo.list
%config %_sysconfdir/apt/vendors.list.d/nixtux-keys.list
%_datadir/nixtux-repo/public-keys/mikhailnov_pub.gpg
%_prefix/lib/rpm/nixtux-keys.filetrigger
#%doc README

%changelog
* Fri Jul 27 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.4-alt1
- add p8/x86_64-i586 blobs
* Fri Jul 27 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.3-alt2
- Rebuild in p8
* Fri Jul 13 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.3-alt1
- add RPMS.classic
* Tue Jul 10 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.2-alt1
- Fixes (it works!)
* Tue Jul 10 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.1-alt11
- initial build

